API Endpoints and Relations
###########################

This section focuses on some of the core relations present in the application's database schema which
are responsible for data storage. It also highlights some of the fundamental API endpoints through
which user requests or responses are fulfilled while interacting with the application.



API Endpoints
*************
Listed and described below are some fundamental endpoints that enable the system to fetch or store 
data to maintain overall consistency.


Registration
============

https://bridge.innovationagency.go.ke/api/register
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint facilitates creation of new accounts therefore admitting new users to the KIB platform.
User details submitted during the sign up phase are submitted via a *POST* request to the *store* method
situated in the *UserController*. This ensures that the user details are stored in the application's 
database. Below is a depiction of the data submitted to the server via this endpoint.

    ..  figure::  ../../images/RegistrationPayload.png
        :class: with-border
        :alt: Registration Payload
        :scale: 80
        :align: center

        *Figure 1: Registration payload*



Login
=====

https://bridge.innovationagency.go.ke/api/login
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

During the login phase, this endpoint receives user credentials(*email and password*) to initiate a 
*POST* method that sends the login request to the server for authentication and validation with 
the aid of some methods within the *LoginController*.
Authentication ensures that the requesting user is an exisiting user with relevant permissions to initiate
the intended request whereas validation ensures validity of the credentials keyed in.

    ..  figure::  ../../images/LoginPayload.png
        :class: with-border
        :alt: Login Payload
        :scale: 80
        :align: center

        *Figure 2: Login payload*


Dashboard
=========

https://bridge.innovationagency.go.ke/api/dashboard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint bears data pertaining to most entities within KIB such as innovations, institutions
etc. Through a *GET* request, this information is availed and summarized via visuals 
like bar graphs and pie charts whenever the user navigates the *Dashboard Module*.

    ..  figure::  ../../images/DashboardStatsHeader.png
        :class: with-border
        :alt: Dashboard Header
        :scale: 80
        :align: center

        *Figure 3: Request header - Dashboard stats*


Innovations
===========

https://bridge.innovationagency.go.ke/api/innovations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Through this endpoint, data access and manipulation may be performed in relation to the
innovations module. The example below depicts the consequence of a *GET* request on this endpoint.
The data returned represents the various innovations within the *Popular Now* category.

    ..  figure::  ../../images/InnovationsPopularHeader.png
        :class: with-border
        :alt: Popular Innovations
        :scale: 80
        :align: center

        *Figure 4: GET request - Popular innovations*


Organisations
=============

https://bridge.innovationagency.go.ke/api/organisations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint enables navigation through the organisation entity as wells as data manipulation operations 
involving them.
Depicted below is an illustration of a request header that seeks to withdraw an organisation from the KIB platform.

    ..  figure::  ../../images/OrgDeletionHeader.png
        :class: with-border
        :alt: Deleting an organisation
        :scale: 80
        :align: center

        *Figure 5: HTTP DELETE method - Organisations*


Institutions
============

https://bridge.innovationagency.go.ke/api/institutions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Access and mutation of data pertaining to institutions is enabled via this route.

The snippet below exhibits a request header that updates the details of an existing
institution via a *PUT* request.

    ..  figure::  ../../images/CreateInsitution.png
        :class: with-border
        :alt: Creating an instituion
        :scale: 80
        :align: center

        *Figure 6: HTTP PUT method - Institutions*



Users
=====

https://bridge.innovationagency.go.ke/api/users
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint grants access and modification possibilities on user accounts. Below is a request header 
that intends to update a user's account via a *PUT* request.

    ..  figure::  ../../images/UserUpdateHeader.png
        :class: with-border
        :alt: Updating a user
        :scale: 80
        :align: center

        *Figure 7: HTTP PUT method - Users*

|

Relations
*********
The relations in KIB's database abide by the following conventions:

1. Column names corresponding to the primary key are labeled with **(PK)**.
2. Column names correspondng to the foreign key are labeled with **(FK, table_referenced.column_referenced)**.


Calls
=====

calls
^^^^^

+----------------+---------------+
| Column         | Data Type     |
+================+===============+
| id(PK)         | BIGINT(20)    |
+----------------+---------------+
| title          | VARCHAR(255)  |
+----------------+---------------+
| brief          | LONGTEXT      |
+----------------+---------------+
| description    | LONGTEXT      |
+----------------+---------------+
| featured_image | VARCHAR(255)  |
+----------------+---------------+
| action_url     | VARCHAR(255)  |
+----------------+---------------+
| deadline       | DATETIME      |
+----------------+---------------+
| slug           | VARCHAR(255)  |
+----------------+---------------+
| status         | TINYINT(1)    |
+----------------+---------------+
| visibility     | TINYINT(1)    |
+----------------+---------------+
| created_at     | TIMESTAMP     |
+----------------+---------------+
| updated_at     | TIMESTAMP     |
+----------------+---------------+
| deleted_at     | TIMESTAMP     |
+----------------+---------------+


call_dates
^^^^^^^^^^

+------------------------------+---------------+
| Column                       | Data Type     |
+==============================+===============+
| id(PK)                       | BIGINT(20)    |
+------------------------------+---------------+
| call_id(FK, calls.id)        | BIGINT(20)    |
+------------------------------+---------------+
| name                         | VARCHAR(255)  |
+------------------------------+---------------+
| description                  | VARCHAR(255)  |
+------------------------------+---------------+


call_interests
^^^^^^^^^^^^^^

+--------------------------------------+---------------+
| Column                               | Data Type     |
+======================================+===============+
| id(PK)                               | BIGINT(20)    |
+--------------------------------------+---------------+
| call_id(FK, calls.id)                | BIGINT(20)    |
+--------------------------------------+---------------+
| interest_id(FK, interests.id)        | BIGINT(20)    |
+--------------------------------------+---------------+


call_responses
^^^^^^^^^^^^^^

+------------------------------+---------------+
| Column                       | Data Type     |
+==============================+===============+
| id(PK)                       | BIGINT(20)    |
+------------------------------+---------------+
| call_id(FK, calls.id)        | BIGINT(20)    |
+------------------------------+---------------+
| user_id(FK, users.id)        | BIGINT(20)    |
+------------------------------+---------------+
| challenge_addressed          | LONGTEXT      |
+------------------------------+---------------+
| one_line_summary             | LONGTEXT      |
+------------------------------+---------------+
| problem_solved               | LONGTEXT      |
+------------------------------+---------------+
| solution                     | LONGTEXT      |
+------------------------------+---------------+
| target_audience              | LONGTEXT      |
+------------------------------+---------------+
| positioning                  | LONGTEXT      |
+------------------------------+---------------+
| application_reason           | LONGTEXT      |
+------------------------------+---------------+
| innovative                   | LONGTEXT      |
+------------------------------+---------------+
| impact_goals                 | LONGTEXT      |
+------------------------------+---------------+
| impact_goal_measure          | LONGTEXT      |
+------------------------------+---------------+
| change_theory                | LONGTEXT      |
+------------------------------+---------------+
| core_technology              | LONGTEXT      |
+------------------------------+---------------+
| incorporation_approach       | LONGTEXT      |
+------------------------------+---------------+
| business_model               | LONGTEXT      |
+------------------------------+---------------+
| financial_stability          | LONGTEXT      |
+------------------------------+---------------+
| innovation_link              | VARCHAR(255)  |
+------------------------------+---------------+
| created_at                   | TIMESTAMP     |
+------------------------------+---------------+
| updated_at                   | TIMESTAMP     |
+------------------------------+---------------+


call_sdgs
^^^^^^^^^

+------------------------------+-------------+
| Column                       | Data Type   |
+==============================+=============+
| id(PK)                       | BIGINT(20)  |
+------------------------------+-------------+
| call_id(FK, calls.id)        | BIGINT(20)  |
+------------------------------+-------------+
| sdg_id(FK, sdgs.id)          | BIGINT(20)  |
+------------------------------+-------------+



call_sectors
^^^^^^^^^^^^

+----------------------------------+-------------+
| Column                           | Data Type   |
+==================================+=============+
| id(PK)                           | BIGINT(20)  |
+----------------------------------+-------------+
| call_id(FK, calls.id)            | BIGINT(20)  |
+----------------------------------+-------------+
| sector_id(FK, sectors.id)        | BIGINT(20)  |
+----------------------------------+-------------+



Chats
=====

chats
^^^^^

+-----------------------------------+-------------+
| Column                            | Data Type   |
+===================================+=============+
| id(PK)                            | BIGINT(20)  |
+-----------------------------------+-------------+
| sender_id(FK, users.id)           | BIGINT(20)  |
+-----------------------------------+-------------+
| recipient_id(FK, users.id)        | BIGINT(20)  |
+-----------------------------------+-------------+
| last_message                      | DATETIME    |
+-----------------------------------+-------------+



chat_orders
^^^^^^^^^^^

+------------------------------+-------------+
| Column                       | Data Type   |
+==============================+=============+
| id(PK)                       | BIGINT(20)  |
+------------------------------+-------------+
| chat_id(FK, chats.id)        | BIGINT(20)  |
+------------------------------+-------------+
| user_id                      | BIGINT(10)  |
+------------------------------+-------------+
| message_id                   | INT(10)     |
+------------------------------+-------------+
| created_at                   | TIMESTAMP   |
+------------------------------+-------------+
| updated_at                   | TIMESTAMP   |
+------------------------------+-------------+


Innovations
===========

innovations
^^^^^^^^^^^

+--------------------------------------------------+---------------+
| Column                                           | Data Type     |
+==================================================+===============+
| id(PK)                                           | BIGINT(20)    |
+--------------------------------------------------+---------------+
| name                                             | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| brief                                            | LONGTEXT      |
+--------------------------------------------------+---------------+
| establishment_year                               | INT(10)       |
+--------------------------------------------------+---------------+
| ip_protection_type_id(FK, ip_protection_types.id)| BIGINT(20)    |
+--------------------------------------------------+---------------+
| stage_id(FK,  stages.id)                         | BIGINT(20)    |
+--------------------------------------------------+---------------+
| county_id(FK, counties.id)                       | BIGINT(20)    |
+--------------------------------------------------+---------------+
| description                                      | LONGTEXT      |
+--------------------------------------------------+---------------+
| featured_image                                   | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| slug                                             | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| average_rating                                   | DOUBLE        |
+--------------------------------------------------+---------------+
| total_rating                                     | INT(10)       |
+--------------------------------------------------+---------------+
| created_by(FK, users.id)                         | BIGINT(20)    |
+--------------------------------------------------+---------------+
| views                                            | INT(10)       |
+--------------------------------------------------+---------------+
| rating_public                                    | TINYINT(1)    |
+--------------------------------------------------+---------------+
| status                                           | TINYINT(1)    |
+--------------------------------------------------+---------------+
| verified                                         | TINYINT(1)    |
+--------------------------------------------------+---------------+
| created_at                                       | TIMESTAMP     |
+--------------------------------------------------+---------------+
| updated_at                                       | TIMESTAMP     |
+--------------------------------------------------+---------------+
| deleted_at                                       | TIMESTAMP     |
+--------------------------------------------------+---------------+


innovation_administrators
^^^^^^^^^^^^^^^^^^^^^^^^^

+----------------------------------+---------------+
| Column                           | Data Type     |
+==================================+===============+
| id(PK)                           | BIGINT(20)    |
+----------------------------------+---------------+
| innovation_id(FK, innovations.id)| BIGINT(20)    |
+----------------------------------+---------------+
| user_id(FK, users.id)            | BIGINT(20)    |
+----------------------------------+---------------+
| role                             | VARCHAR(255)  |
+----------------------------------+---------------+
| write                            | TINYINT(1)    |
+----------------------------------+---------------+
| status                           | TINYINT(10)   |
+----------------------------------+---------------+
| visible                          | TINYINT(1)    |
+----------------------------------+---------------+
| created_at                       | TIMESTAMP     |
+----------------------------------+---------------+
| updated_at                       | TIMESTAMP     |
+----------------------------------+---------------+


innovation_asks
^^^^^^^^^^^^^^^

+-----------------------------------+-------------+
| Column                            | Data Type   |
+===================================+=============+
| id(PK)                            | BIGINT(20)  |
+-----------------------------------+-------------+
| innovation_id(FK. innovations.id) | BIGINT(20)  |
+-----------------------------------+-------------+
| ask_id(FK. asks.id)               | BIGINT(20)  |
+-----------------------------------+-------------+




innovation_connections
^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------+-------------+
| Column                            | Data Type   |
+===================================+=============+
| id(PK)                            | BIGINT(20)  |
+-----------------------------------+-------------+
| innovation_id(FK, innovations.id) | BIGINT(20)  |
+-----------------------------------+-------------+
| friendship_id                     | INT(10)     |
+-----------------------------------+-------------+
| created_at                        | TIMESTAMP   |
+-----------------------------------+-------------+
| updated_at                        | TIMESTAMP   |
+-----------------------------------+-------------+


innovation_institutions
^^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+-------------+
| Column                              | Data Type   |
+=====================================+=============+
| id(PK)                              | BIGINT(20)  |
+-------------------------------------+-------------+
| innovation_id(FK, innovations.id)   | BIGINT(20)  |
+-------------------------------------+-------------+
| institution_id(FK, institutions.id) | BIGINT(20)  |
+-------------------------------------+-------------+
| status                              | TINYINT(1)  |
+-------------------------------------+-------------+
| created_at                          | TIMESTAMP   |
+-------------------------------------+-------------+
| updated_at                          | TIMESTAMP   |
+-------------------------------------+-------------+


innovation_interests
^^^^^^^^^^^^^^^^^^^^

+-----------------------------------+-------------+
| Column                            | Data Type   |
+===================================+=============+
| id(PK)                            | BIGINT(20)  |
+-----------------------------------+-------------+
| innovation_id(FK, innovations.id) | BIGINT(20)  |
+-----------------------------------+-------------+
| interest_id(FK, interests.id)     | BIGINT(20)  |
+-----------------------------------+-------------+


innovation_organizations
^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------+------------+
| Column                                | Data Type  |
+=======================================+============+
| id(PK)                                | BIGINT(20) |
+---------------------------------------+------------+
| innovation_id(FK, innovations.id)     | BIGINT(20) |
+---------------------------------------+------------+
| organisation_id(FK, organisations.id) | BIGINT(20) |
+---------------------------------------+------------+
| status                                | TINYINT(1) |
+---------------------------------------+------------+
| created_at                            | TIMESTAMP  |
+---------------------------------------+------------+
| updated_at                            | TIMESTAMP  |
+---------------------------------------+------------+


innovation_recommendations
^^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------+------------+
| Column                            | Data Type  |
+===================================+============+
| id(PK)                            | BIGINT(20) |
+-----------------------------------+------------+
| innovation_id(FK, innovations.id) | BIGINT(20) |
+-----------------------------------+------------+
| user_id(FK, users.id)             | BIGINT(20) |
+-----------------------------------+------------+
| score                             | BIGINT(20) |
+-----------------------------------+------------+


innovation_sdgs
^^^^^^^^^^^^^^^

+-----------------------------------+------------+
| Column                            | Data Type  |
+===================================+============+
| id(PK)                            | BIGINT(20) |
+-----------------------------------+------------+
| innovation_id(FK, innovations.id) | BIGINT(20) |
+-----------------------------------+------------+
| sdg_id(FK, sdgs.id)               | BIGINT(20) |
+-----------------------------------+------------+


innovation_sectors
^^^^^^^^^^^^^^^^^^

+-----------------------------------+------------+
| Column                            | Data Type  |
+===================================+============+
| id(PK)                            | BIGINT(20) |
+-----------------------------------+------------+
| innovation_id(FK, innovations.id) | BIGINT(20) |
+-----------------------------------+------------+
| sector_id(FK, sectors.id)         | BIGINT(20) |
+-----------------------------------+------------+

innovation_socials
^^^^^^^^^^^^^^^^^^

+-----------------------------------+--------------+
| Column                            | Data Type    |
+===================================+==============+
| id(PK)                            | BIGINT(20)   |
+-----------------------------------+--------------+
| innovation_id(FK, innovations.id) | BIGINT(20)   |
+-----------------------------------+--------------+
| email                             | VARCHAR(255) |
+-----------------------------------+--------------+
| phone                             | VARCHAR(255) |
+-----------------------------------+--------------+
| web_url                           | VARCHAR(255) |
+-----------------------------------+--------------+
| fb_url                            | VARCHAR(255) |
+-----------------------------------+--------------+
| insta_url                         | VARCHAR(255) |
+-----------------------------------+--------------+
| twitter_url                       | VARCHAR(255) |
+-----------------------------------+--------------+
| linkedin_url                      | VARCHAR(255) |
+-----------------------------------+--------------+


innovation_views
^^^^^^^^^^^^^^^^

+-----------------------------------+------------+
| Column                            | Data Type  |
+===================================+============+
| id(PK)                            | BIGINT(20) |
+-----------------------------------+------------+
| innovation_id(FK, innovations.id) | BIGINT(20) |
+-----------------------------------+------------+
| user_id(FK, users.id)             | BIGINT(20) |
+-----------------------------------+------------+
| created_at                        | TIMESTAMP  |
+-----------------------------------+------------+
| updated_at                        | TIMESTAMP  |
+-----------------------------------+------------+



Institutions
============

institutions
^^^^^^^^^^^^

+-----------------------------------------------+--------------+
| Column                                        | DataTType    |
+===============================================+==============+
| id(PK)                                        | BIGINT(20)   |
+-----------------------------------------------+--------------+
| name                                          | VARCHAR(255) |
+-----------------------------------------------+--------------+
| brief                                         | LONGTEXT     |
+-----------------------------------------------+--------------+
| description                                   | LONGTEXT     |
+-----------------------------------------------+--------------+
| logo                                          | VARCHAR(255) |
+-----------------------------------------------+--------------+
| cover_image                                   | VARCHAR(255) |
+-----------------------------------------------+--------------+
| institution_type_id(FK. institution_types.id) | BIGINT(20)   |
+-----------------------------------------------+--------------+
| physical_address                              | VARCHAR(255) |
+-----------------------------------------------+--------------+
| county_id(FK. counties.id)                    | BIGINT(20)   |
+-----------------------------------------------+--------------+
| slug                                          | VARCHAR(255) |
+-----------------------------------------------+--------------+
| created_by                                    | BIGINT(20)   |
+-----------------------------------------------+--------------+
| status                                        | TINYINT(1)   |
+-----------------------------------------------+--------------+
| created_at                                    | TIMESTAMP    |
+-----------------------------------------------+--------------+
| updated_at                                    | TIMESTAMP    |
+-----------------------------------------------+--------------+
| deleted_at                                    | TIMESTAMP    |
+-----------------------------------------------+--------------+


institution administrators
^^^^^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| user_id                             | BIGINT(20) |
+-------------------------------------+------------+
| status                              | TINYINT(1) |
+-------------------------------------+------------+
| created_at                          | TIMESTAMP  |
+-------------------------------------+------------+
| updated_at                          | TIMESTAMP  |
+-------------------------------------+------------+


institution_calls
^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| call_id(FK, calls.id)               | BIGINT(20) |
+-------------------------------------+------------+


institution_contacts
^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+--------------+
| Column                              | Data Type    |
+=====================================+==============+
| id(PK)                              | BIGINT(20)   |
+-------------------------------------+--------------+
| institution_id(FK, institutions.id) | BIGINT(20)   |
+-------------------------------------+--------------+
| contact_person                      | VARCHAR(255) |
+-------------------------------------+--------------+
| email                               | VARCHAR(255) |
+-------------------------------------+--------------+
| email_2                             | VARCHAR(255) |
+-------------------------------------+--------------+
| phone                               | VARCHAR(255) |
+-------------------------------------+--------------+
| phone_2                             | VARCHAR(255) |
+-------------------------------------+--------------+
| web_url                             | VARCHAR(255) |
+-------------------------------------+--------------+
| fb_url                              | VARCHAR(255) |
+-------------------------------------+--------------+
| insta_url                           | VARCHAR(255) |
+-------------------------------------+--------------+
| twitter_url                         | VARCHAR(255) |
+-------------------------------------+--------------+
| linkedin_url                        | VARCHAR(255) |
+-------------------------------------+--------------+
| visible                             | TINYINT(1)   |
+-------------------------------------+--------------+


institution_events
^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| event_id(FK, events.id)             | BIGINT(20) |
+-------------------------------------+------------+


institution_interest_stats
^^^^^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| interest_id(FK, interestts.id)      | BIGINT(20) |
+-------------------------------------+------------+
| total                               | INT(10)    |
+-------------------------------------+------------+
| day                                 | INT(10)    |
+-------------------------------------+------------+
| week                                | INT(10)    |
+-------------------------------------+------------+
| month                               | INT(10)    |
+-------------------------------------+------------+
| year                                | INT(10)    |
+-------------------------------------+------------+


institution_sdg_stats
^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| sdg_id(FK, sdgs.id)                 | BIGINT(20) |
+-------------------------------------+------------+
| total                               | INT(10)    |
+-------------------------------------+------------+
| day                                 | INT(10)    |
+-------------------------------------+------------+
| week                                | INT(10)    |
+-------------------------------------+------------+
| month                               | INT(10)    |
+-------------------------------------+------------+
| year                                | INT(10)    |
+-------------------------------------+------------+


institution_sectors
^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| sector_id(FK, sectors.id)           | BIGINT(20) |
+-------------------------------------+------------+


institution_sector_stats
^^^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| sector_id(FK, sectors.id)           | BIGINT(20) |
+-------------------------------------+------------+
| total                               | INT(10)    |
+-------------------------------------+------------+
| day                                 | INT(10)    |
+-------------------------------------+------------+
| week                                | INT(10)    |
+-------------------------------------+------------+
| month                               | INT(10)    |
+-------------------------------------+------------+
| year                                | INT(10)    |
+-------------------------------------+------------+

institution_statistics
^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| total_innovations                   | INT(10)    |
+-------------------------------------+------------+
| day_innovations                     | INT(10)    |
+-------------------------------------+------------+
| week_innovations                    | INT(10)    |
+-------------------------------------+------------+
| month_innovations                   | INT(10)    |
+-------------------------------------+------------+
| year_innovations                    | INT(10)    |
+-------------------------------------+------------+
| total_users                         | INT(10)    |
+-------------------------------------+------------+
| day_users                           | INT(10)    |
+-------------------------------------+------------+
| week_users                          | INT(10)    |
+-------------------------------------+------------+
| month_users                         | INT(10)    |
+-------------------------------------+------------+
| year_users                          | INT(10)    |
+-------------------------------------+------------+
| total_connections                   | INT(10)    |
+-------------------------------------+------------+
| day_connections                     | INT(10)    |
+-------------------------------------+------------+
| week_connections                    | INT(10)    |
+-------------------------------------+------------+
| month_connections                   | INT(10)    |
+-------------------------------------+------------+
| year_connections                    | INT(10)    |
+-------------------------------------+------------+


institution_types
^^^^^^^^^^^^^^^^^

+--------+--------------+
| Column | Data Type    |
+========+==============+
| id(PK) | BIGINT(20)   |
+--------+--------------+
| name   | VARCHAR(255) |
+--------+--------------+


institution_users
^^^^^^^^^^^^^^^^^

+-------------------------------------+------------+
| Column                              | Data Type  |
+=====================================+============+
| id(PK)                              | BIGINT(20) |
+-------------------------------------+------------+
| institution_id(FK, institutions.id) | BIGINT(20) |
+-------------------------------------+------------+
| user_id(FK, users.id)               | BIGINT(20) |
+-------------------------------------+------------+
| status                              | TINYINT(1) |
+-------------------------------------+------------+
| created_at                          | TIMESTAMP  |
+-------------------------------------+------------+
| updated_at                          | TIMESTAMP  |
+-------------------------------------+------------+


ins_inst_affiliations
^^^^^^^^^^^^^^^^^^^^^

+------------------------------------------------+------------+
| Column                                         | Data Type  |
+================================================+============+
| id(PK)                                         | BIGINT(20) |
+------------------------------------------------+------------+
| institution_id(FK, institutions.id)            | BIGINT(20) |
+------------------------------------------------+------------+
| aff_institution_id(FK, institutions.id)        | BIGINT(20) |
+------------------------------------------------+------------+
| status                                         | TINYINT(1) |
+------------------------------------------------+------------+
| requesting_institution_id(FK, institutions.id) | BIGINT(20) |
+------------------------------------------------+------------+


ins_org_affiliations
^^^^^^^^^^^^^^^^^^^^

+------------------------------------------------+------------+
| Column                                         | Data Type  |
+================================================+============+
| id(PK)                                         | BIGINT(20) |
+------------------------------------------------+------------+
| institution_id(FK, institutions.id)            | BIGINT(20) |
+------------------------------------------------+------------+
| aff_organisation_id(FK, organisations.id)      | BIGINT(20) |
+------------------------------------------------+------------+
| status                                         | TINYINT(1) |
+------------------------------------------------+------------+
| requesting_institution_id(FK, institutions.id) | BIGINT(20) |
+------------------------------------------------+------------+



Investments
===========

investments
^^^^^^^^^^^

+---------------------------------------+------------+
| Column                                | DataTType  |
+=======================================+============+
| id(PK)                                | BIGINT(20) |
+---------------------------------------+------------+
| innovation_id(FK, innovations.id)     | BIGINT(20) |
+---------------------------------------+------------+
| funding_type_id(FK, funding_types.id) | BIGINT(20) |
+---------------------------------------+------------+
| amount                                | INT(10)    |
+---------------------------------------+------------+
| status                                | INT(10)    |
+---------------------------------------+------------+
| public                                | TINYINT(1) |
+---------------------------------------+------------+
| created_at                            | TIMESTAMP  |
+---------------------------------------+------------+
| updated_at                            | TIMESTAMP  |
+---------------------------------------+------------+



investment_firms
^^^^^^^^^^^^^^^^

+-----------------------------------------+--------------+
| Column                                  | Data Type    |
+=========================================+==============+
| id(PK)                                  | BIGINT(20)   |
+-----------------------------------------+--------------+
| name                                    | VARCHAR(255) |
+-----------------------------------------+--------------+
| brief                                   | LONGTEXT     |
+-----------------------------------------+--------------+
| description                             | LONGTEXT     |
+-----------------------------------------+--------------+
| logo                                    | VARCHAR(255) |
+-----------------------------------------+--------------+
| cover_image                             | VARCHAR(255) |
+-----------------------------------------+--------------+
| investor_type_id(FK, investor_types.id) | BIGINT(20)   |
+-----------------------------------------+--------------+
| establishment_year                      | VARCHAR(255) |
+-----------------------------------------+--------------+
| physical_address                        | VARCHAR(255) |
+-----------------------------------------+--------------+
| county_id(FK, counties.id)              | BIGINT(20)   |
+-----------------------------------------+--------------+
| slug                                    | VARCHAR(255) |
+-----------------------------------------+--------------+
| created_by(FK, users.id)                | BIGINT(20)   |
+-----------------------------------------+--------------+
| status                                  | TINYINT(1)   |
+-----------------------------------------+--------------+
| created_at                              | TIMESTAMP    |
+-----------------------------------------+--------------+
| updated_at                              | TIMESTAMP    |
+-----------------------------------------+--------------+
| deleted_at                              | TIMESTAMP    |
+-----------------------------------------+--------------+


investment_firm_administrators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------------+------------+
| Column                                      | Data Type  |
+=============================================+============+
| id(PK)                                      | BIGINT(20) |
+---------------------------------------------+------------+
| investment_firm_id(FK, investment_firms.id) | BIGINT(20) |
+---------------------------------------------+------------+
| user_id(FK, users.id)                       | BIGINT(20) |
+---------------------------------------------+------------+
| status                                      | TINYINT(1) |
+---------------------------------------------+------------+
| write                                       | TINYINT(1) |
+---------------------------------------------+------------+
| created_at                                  | TIMESTAMP  |
+---------------------------------------------+------------+
| updated_at                                  | TIMESTAMP  |
+---------------------------------------------+------------+


investment_firm_contacts
^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------------+--------------+
| Column                                      | Data Type    |
+=============================================+==============+
| id(PK)                                      | BIGINT(20)   |
+---------------------------------------------+--------------+
| investment_firm_id(FK, investment_firms.id) | BIGINT(20)   |
+---------------------------------------------+--------------+
| contact_person                              | VARCHAR(255) |
+---------------------------------------------+--------------+
| email                                       | VARCHAR(255) |
+---------------------------------------------+--------------+
| email_2                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| phone                                       | VARCHAR(255) |
+---------------------------------------------+--------------+
| phone_2                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| web_url                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| fb_url                                      | VARCHAR(255) |
+---------------------------------------------+--------------+
| insta_url                                   | VARCHAR(255) |
+---------------------------------------------+--------------+
| twitter_url                                 | VARCHAR(255) |
+---------------------------------------------+--------------+
| linkedin_url                                | VARCHAR(255) |
+---------------------------------------------+--------------+
| visible                                     | TINYINT(1)   |
+---------------------------------------------+--------------+

investment_firm_investments
^^^^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------------+------------+
| Column                                      | Data Type  |
+=============================================+============+
| id(PK)                                      | BIGINT(20) |
+---------------------------------------------+------------+
| investment_firm_id(FK, investment_firms.id) | BIGINT(20) |
+---------------------------------------------+------------+
| investment_id(FK, investments.id)           | BIGINT(20) |
+---------------------------------------------+------------+

investment_firm_watchlists
^^^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------------+------------+
| Column                                      | Data Type  |
+=============================================+============+
| id(PK)                                      | BIGINT(20) |
+---------------------------------------------+------------+
| investment_firm_id(FK, investment_firms.id) | BIGINT(20) |
+---------------------------------------------+------------+
| innovation_id(FK, innovations.id)           | BIGINT(20) |
+---------------------------------------------+------------+
| created_by                                  | BIGINT(20) |
+---------------------------------------------+------------+
| created_at                                  | TIMESTAMP  |
+---------------------------------------------+------------+
| updated_at                                  | TIMESTAMP  |
+---------------------------------------------+------------+



Investor
========

investor_innovations
^^^^^^^^^^^^^^^^^^^^

+-----------------------------------+------------+
| Column                            | DataTType  |
+===================================+============+
| id(PK)                            | BIGINT(20) |
+-----------------------------------+------------+
| user_id(FK, users.id)             | BIGINT(20) |
+-----------------------------------+------------+
| innovation_id(FK, innovations.id) | BIGINT(20) |
+-----------------------------------+------------+
| created_at                        | TIMESTAMP  |
+-----------------------------------+------------+
| updated_at                        | TIMESTAMP  |
+-----------------------------------+------------+


investor_innovation_notes
^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------+--------------+
| Column                            | Data Type    |
+===================================+==============+
| id(PK)                            | BIGINT(20)   |
+-----------------------------------+--------------+
| user_id(FK, users.id)             | BIGINT(20)   |
+-----------------------------------+--------------+
| innovation_id(FK, innovations.id) | BIGINT(20)   |
+-----------------------------------+--------------+
| title                             | VARCHAR(255) |
+-----------------------------------+--------------+
| content                           | LONGTEXT     |
+-----------------------------------+--------------+
| created_at                        | TIMESTAMP    |
+-----------------------------------+--------------+
| updated_at                        | TIMESTAMP    |
+-----------------------------------+--------------+


investor_types
^^^^^^^^^^^^^^
+--------+--------------+
| Column | Data Type    |
+========+==============+
| id(PK) | BIGINT(20)   |
+--------+--------------+
| name   | VARCHAR(255) |
+--------+--------------+



Organisations
=============

organisations
^^^^^^^^^^^^^
+------------------------------------------------+--------------+
| Column                                         | Data Type    |
+================================================+==============+
| id(PK)                                         | BIGINT(20)   |
+------------------------------------------------+--------------+
| name                                           | VARCHAR(255) |
+------------------------------------------------+--------------+
| brief                                          | LONGTEXT     |
+------------------------------------------------+--------------+
| description                                    | LONGTEXT     |
+------------------------------------------------+--------------+
| logo                                           | VARCHAR(255) |
+------------------------------------------------+--------------+
| cover_image                                    | VARCHAR(255) |
+------------------------------------------------+--------------+
| organisation_type_id(FK, organisation_types.id)| BIGINT(20)   |
+------------------------------------------------+--------------+
| establishment_year                             | VARCHAR(255) |
+------------------------------------------------+--------------+
| physical_address                               | VARCHAR(255) |
+------------------------------------------------+--------------+
| county_id(FK, counties.id)                     | BIGINT(20)   |
+------------------------------------------------+--------------+
| slug                                           | VARCHAR(255) |
+------------------------------------------------+--------------+
| created_by(FK, users.id)                       | BIGINT(20)   |
+------------------------------------------------+--------------+
| status                                         | TINYINT(1)   |
+------------------------------------------------+--------------+
| created_at                                     | TIMESTAMP    |
+------------------------------------------------+--------------+
| updated_at                                     | TIMESTAMP    |
+------------------------------------------------+--------------+
| deleted_at                                     | TIMESTAMP    |
+------------------------------------------------+--------------+


organisation_administrators
^^^^^^^^^^^^^^^^^^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| user_id(FK, users.id)                | BIGINT(20) |
+--------------------------------------+------------+
| status                               | TINYINT(1) |
+--------------------------------------+------------+
| created_at                           | TIMESTAMP  |
+--------------------------------------+------------+
| updated_at                           | TIMESTAMP  |
+--------------------------------------+------------+


organisation_calls
^^^^^^^^^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| call_id(FK, calls.id)                | BIGINT(20) |
+--------------------------------------+------------+


organisation_calls
^^^^^^^^^^^^^^^^^^
+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| call_id(FK, calls.id)                | BIGINT(20) |
+--------------------------------------+------------+


organisation_contacts
^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------------+--------------+
| Column                                      | Data Type    |
+=============================================+==============+
| id(PK)                                      | BIGINT(20)   |
+---------------------------------------------+--------------+
| organisation_id(FK, organisations.id)       | BIGINT(20)   |
+---------------------------------------------+--------------+
| contact_person                              | VARCHAR(255) |
+---------------------------------------------+--------------+
| email                                       | VARCHAR(255) |
+---------------------------------------------+--------------+
| email_2                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| phone                                       | VARCHAR(255) |
+---------------------------------------------+--------------+
| phone_2                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| web_url                                     | VARCHAR(255) |
+---------------------------------------------+--------------+
| fb_url                                      | VARCHAR(255) |
+---------------------------------------------+--------------+
| insta_url                                   | VARCHAR(255) |
+---------------------------------------------+--------------+
| twitter_url                                 | VARCHAR(255) |
+---------------------------------------------+--------------+
| linkedin_url                                | VARCHAR(255) |
+---------------------------------------------+--------------+
| visible                                     | TINYINT(1)   |
+---------------------------------------------+--------------+


organisation_events
^^^^^^^^^^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | DataTType  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| event_id(FK, events.id)              | BIGINT(20) |
+--------------------------------------+------------+


organisation_interest_stats
^^^^^^^^^^^^^^^^^^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| interest_id(FK, interestts.id)       | BIGINT(20) |
+--------------------------------------+------------+
| total                                | INT(10)    |
+--------------------------------------+------------+
| day                                  | INT(10)    |
+--------------------------------------+------------+
| week                                 | INT(10)    |
+--------------------------------------+------------+
| month                                | INT(10)    |
+--------------------------------------+------------+
| year                                 | INT(10)    |
+--------------------------------------+------------+


organisation_sdgs
^^^^^^^^^^^^^^^^^

+---------------------------------------+-------------+
| Column                                | Data Type   |
+=======================================+=============+
| id(PK)                                | BIGINT(20)  |
+---------------------------------------+-------------+
| organisation_id(FK, organisations.id) | BIGINT(20)  |
+---------------------------------------+-------------+
| sdg_id(FK, sdgs.id)                   | BIGINT(20)  |
+---------------------------------------+-------------+


organisation_sdg_stats
^^^^^^^^^^^^^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| organisation_id(FK, organisations.id)| BIGINT(20) |
+--------------------------------------+------------+
| sdg_id(FK, sdgs.id)                  | BIGINT(20) |
+--------------------------------------+------------+
| total                                | INT(10)    |
+--------------------------------------+------------+
| day                                  | INT(10)    |
+--------------------------------------+------------+
| week                                 | INT(10)    |
+--------------------------------------+------------+
| month                                | INT(10)    |
+--------------------------------------+------------+
| year                                 | INT(10)    |
+--------------------------------------+------------+


organisation_sectors
^^^^^^^^^^^^^^^^^^^^

+--------------------------------------+-------------+
| Column                               | Data Type   |
+======================================+=============+
| id(PK)                               | BIGINT(20)  |
+--------------------------------------+-------------+
| organisation_id(FK, organisations.id)| BIGINT(20)  |
+--------------------------------------+-------------+
| sector_id(FK, sectors.id)            | BIGINT(20)  |
+--------------------------------------+-------------+


organisation_sector_stats
^^^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------+------------+
| Column                                | Data Type  |
+=======================================+============+
| id(PK)                                | BIGINT(20) |
+---------------------------------------+------------+
| organisation_id(FK, organisations.id) | BIGINT(20) |
+---------------------------------------+------------+
| sector_id(FK, sectors.id)             | BIGINT(20) |
+---------------------------------------+------------+
| total                                 | INT(10)    |
+---------------------------------------+------------+
| day                                   | INT(10)    |
+---------------------------------------+------------+
| week                                  | INT(10)    |
+---------------------------------------+------------+
| month                                 | INT(10)    |
+---------------------------------------+------------+
| year                                  | INT(10)    |
+---------------------------------------+------------+


organisation_statistics
^^^^^^^^^^^^^^^^^^^^^^^

+---------------------------------------+------------+
| Column                                | Data Type  |
+=======================================+============+
| id(PK)                                | BIGINT(20) |
+---------------------------------------+------------+
| organisation_id(FK, organisations.id) | BIGINT(20) |
+---------------------------------------+------------+
| total_innovations                     | INT(10)    |
+---------------------------------------+------------+
| day_innovations                       | INT(10)    |
+---------------------------------------+------------+
| week_innovations                      | INT(10)    |
+---------------------------------------+------------+
| month_innovations                     | INT(10)    |
+---------------------------------------+------------+
| year_innovations                      | INT(10)    |
+---------------------------------------+------------+
| total_users                           | INT(10)    |
+---------------------------------------+------------+
| day_users                             | INT(10)    |
+---------------------------------------+------------+
| week_users                            | INT(10)    |
+---------------------------------------+------------+
| month_users                           | INT(10)    |
+---------------------------------------+------------+
| year_users                            | INT(10)    |
+---------------------------------------+------------+
| total_connections                     | INT(10)    |
+---------------------------------------+------------+
| day_connections                       | INT(10)    |
+---------------------------------------+------------+
| week_connections                      | INT(10)    |
+---------------------------------------+------------+
| month_connections                     | INT(10)    |
+---------------------------------------+------------+
| year_connections                      | INT(10)    |
+---------------------------------------+------------+


organisation_types
^^^^^^^^^^^^^^^^^^

+--------+--------------+
| Column | Data Type    |
+========+==============+
| id(PK) | BIGINT(20)   |
+--------+--------------+
| name   | VARCHAR(255) |
+--------+--------------+


organisation_users
^^^^^^^^^^^^^^^^^^

+---------------------------------------+------------+
| Column                                | Data Type  |
+=======================================+============+
| id(PK)                                | BIGINT(20) |
+---------------------------------------+------------+
| organisation_id(FK, organisations.id) | BIGINT(20) |
+---------------------------------------+------------+
| user_id(FK, users.id)                 | BIGINT(20) |
+---------------------------------------+------------+
| status                                | TINYINT(1) |
+---------------------------------------+------------+
| created_at                            | TIMESTAMP  |
+---------------------------------------+------------+
| updated_at                            | TIMESTAMP  |
+---------------------------------------+------------+


org_inst_affiliations
^^^^^^^^^^^^^^^^^^^^^

+-------------------------------------------------+------------+
| Column                                          | Data Type  |
+=================================================+============+
| id(PK)                                          | BIGINT(20) |
+-------------------------------------------------+------------+
| organisation_id(FK, organisations.id)           | BIGINT(20) |
+-------------------------------------------------+------------+
| aff_institution_id(FK, institutions.id)         | BIGINT(20) |
+-------------------------------------------------+------------+
| status                                          | TINYINT(1) |
+-------------------------------------------------+------------+
| requesting_organisation_id(FK, organisations.id)| BIGINT(20) |
+-------------------------------------------------+------------+


org_org_affiliations
^^^^^^^^^^^^^^^^^^^^

+-------------------------------------------------+------------+
| Column                                          | Data Type  |
+=================================================+============+
| id(PK)                                          | BIGINT(20) |
+-------------------------------------------------+------------+
| organisation_id(FK, organisations.id)           | BIGINT(20) |
+-------------------------------------------------+------------+
| aff_organisation_id(FK, organisations.id)       | BIGINT(20) |
+-------------------------------------------------+------------+
| status                                          | TINYINT(1) |
+-------------------------------------------------+------------+
| requesting_organisation_id(FK, organisations.id)| BIGINT(20) |
+-------------------------------------------------+------------+



Projects
========

projects
^^^^^^^^

+--------------------------------------------------+---------------+
| Column                                           | Data Type     |
+==================================================+===============+
| id(PK)                                           | BIGINT(20)    |
+--------------------------------------------------+---------------+
| name                                             | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| brief                                            | LONGTEXT      |
+--------------------------------------------------+---------------+
| description                                      | LONGTEXT      |
+--------------------------------------------------+---------------+
| featured_image                                   | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| start                                            | DATE          |
+--------------------------------------------------+---------------+
| end                                              | DATE          |
+--------------------------------------------------+---------------+
| status                                           | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| slug                                             | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| contact_person                                   | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| contact_email                                    | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| contact_phone                                    | VARCHAR(255)  |
+--------------------------------------------------+---------------+
| institution_id(FK, institutions.id)              | BIGINT(20)    |
+--------------------------------------------------+---------------+
| created_by(FK, users.id)                         | BIGINT(20)    |
+--------------------------------------------------+---------------+
| created_at                                       | TIMESTAMP     |
+--------------------------------------------------+---------------+
| updated_at                                       | TIMESTAMP     |
+--------------------------------------------------+---------------+


project_sdgs
^^^^^^^^^^^^

+------------------------------+-------------+
| Column                       | Data Type   |
+==============================+=============+
| id(PK)                       | BIGINT(20)  |
+------------------------------+-------------+
| project_id(FK, projects.id)  | BIGINT(20)  |
+------------------------------+-------------+
| sdg_id(FK, sdgs.id)          | BIGINT(20)  |
+------------------------------+-------------+


project_sectors
^^^^^^^^^^^^^^^

+----------------------------------+-------------+
| Column                           | Data Type   |
+==================================+=============+
| id(PK)                           | BIGINT(20)  |
+----------------------------------+-------------+
| project_id(FK, projects.id)      | BIGINT(20)  |
+----------------------------------+-------------+
| sector_id(FK, sectors.id)        | BIGINT(20)  |
+----------------------------------+-------------+


project_users
^^^^^^^^^^^^^

+----------------------------------+---------------+
| Column                           | Data Type     |
+==================================+===============+
| id(PK)                           | BIGINT(20)    |
+----------------------------------+---------------+
| project_id(FK, projects.id)      | BIGINT(20)    |
+----------------------------------+---------------+
| user_id(FK, users.id)            | BIGINT(20)    |
+----------------------------------+---------------+
| role                             | VARCHAR(255)  |
+----------------------------------+---------------+



Users
=====


users
^^^^^

+------------------+--------------+
| Column           | Data Type    |
+==================+==============+
| id(PK)           | BIGINT(20)   |
+------------------+--------------+
| f_name           | VARCHAR(255) |
+------------------+--------------+
| m_name           | VARCHAR(255) |
+------------------+--------------+
| l_name           | VARCHAR(255) |
+------------------+--------------+
| email            | VARCHAR(255) |
+------------------+--------------+
| phone            | VARCHAR(255) |
+------------------+--------------+
| password         | VARCHAR(255) |
+------------------+--------------+
| code             | VARCHAR(255) |
+------------------+--------------+
| display_img      | VARCHAR(255) |
+------------------+--------------+
| bio              | VARCHAR(255) |
+------------------+--------------+
| active           | TINYINT(1)   |
+------------------+--------------+
| public           | TINYINT(1)   |
+------------------+--------------+
| activation_token | VARCHAR(255) |
+------------------+--------------+
| created_at       | TIMESTAMP    |
+------------------+--------------+
| updated_at       | TIMESTAMP    |
+------------------+--------------+
| deleted_at       | TIMESTAMP    |
+------------------+--------------+


user_calls
^^^^^^^^^^

+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| user_id(FK, users.id)                | BIGINT(20) |
+--------------------------------------+------------+
| call_id(FK, users.id)                | BIGINT(20) |
+--------------------------------------+------------+
| visibility                           | TINYINT(1) |
+--------------------------------------+------------+


user_friendship_groups
^^^^^^^^^^^^^^^^^^^^^^

+--------------------------------------+--------------+
| Column                               | Data Type    |
+======================================+==============+
| friendship_id(FK, friendships.id)    |  INT(10)     |
+--------------------------------------+--------------+
| friend_type                          | VARCHAR(255) |
+--------------------------------------+--------------+
| friend_id                            | BIGINT(20)   |
+--------------------------------------+--------------+
| group_id                             |  INT(1)      |
+--------------------------------------+--------------+


+--------------------------------------+------------+
| Column                               | Data Type  |
+======================================+============+
| id(PK)                               | BIGINT(20) |
+--------------------------------------+------------+
| user_id(FK, users.id)                | BIGINT(20) |
+--------------------------------------+------------+
| institution_id(FK, institutions.id)  | BIGINT(20) |
+--------------------------------------+------------+


user_interests
^^^^^^^^^^^^^^

+--------------------------------------+---------------+
| Column                               | Data Type     |
+======================================+===============+
| id(PK)                               | BIGINT(20)    |
+--------------------------------------+---------------+
| user_id(FK, users.id)                | BIGINT(20)    |
+--------------------------------------+---------------+
| interest_id(FK, interests.id)        | BIGINT(20)    |
+--------------------------------------+---------------+


user_profiles
^^^^^^^^^^^^^

+--------------------------------------+---------------+
| Column                               | Data Type     |
+======================================+===============+
| id(PK)                               | BIGINT(20)    |
+--------------------------------------+---------------+
| user_id(FK, users.id)                | BIGINT(20)    |
+--------------------------------------+---------------+
| county_id(FK, counties.id)           | BIGINT(20)    |
+--------------------------------------+---------------+
| gender_id(FK, genders.id)            | BIGINT(20)    |
+--------------------------------------+---------------+
| dob                                  | DATE          |
+--------------------------------------+---------------+
| newsletter                           | TINYINT(1)    |
+--------------------------------------+---------------+
| created_at                           | TIMESTAMP     |
+--------------------------------------+---------------+
| updated_at                           | TIMESTAMP     |
+--------------------------------------+---------------+


user_sdgs
^^^^^^^^^

+------------------------------+-------------+
| Column                       | Data Type   |
+==============================+=============+
| id(PK)                       | BIGINT(20)  |
+------------------------------+-------------+
| user_id(FK, users.id)        | BIGINT(20)  |
+------------------------------+-------------+
| sdg_id(FK, sdgs.id)          | BIGINT(20)  |
+------------------------------+-------------+


user_sectors
^^^^^^^^^^^^

+------------------------------+-------------+
| Column                       | Data Type   |
+==============================+=============+
| id(PK)                       | BIGINT(20)  |
+------------------------------+-------------+
| user_id(FK, users.id)        | BIGINT(20)  |
+------------------------------+-------------+
| sector_id(FK, sectors.id)    | BIGINT(20)  |
+------------------------------+-------------+


user_socials
^^^^^^^^^^^^

+-----------------------------------+--------------+
| Column                            | Data Type    |
+===================================+==============+
| id(PK)                            | BIGINT(20)   |
+-----------------------------------+--------------+
| user_id(FK, users.id)             | BIGINT(20)   |
+-----------------------------------+--------------+
| web_url                           | VARCHAR(255) |
+-----------------------------------+--------------+
| fb_url                            | VARCHAR(255) |
+-----------------------------------+--------------+
| insta_url                         | VARCHAR(255) |
+-----------------------------------+--------------+
| twitter_url                       | VARCHAR(255) |
+-----------------------------------+--------------+
| linkedin_url                      | VARCHAR(255) |
+-----------------------------------+--------------+
| visible                           | TINYINT(1)   |
+-----------------------------------+--------------+