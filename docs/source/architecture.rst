.. raw:: html

    <style> .red {color:red} </style>

.. role:: red


Architecture
============
The development of KIB conforms to the **MVC** architecture which decomposes an application into three core
components and these include:

1. **Models** - handle data manipulation procedures such as **CRUD** operations.

2. **Views** - fulfill the role of data presentation to users via user interfaces.

3. **Controllers** - govern the flow of data between *Views* and *Models*.

The **MVC** architecture enforces modularity which aids in averting the undesirable event of a 
single point of failure in which one fault may paralyze the entire system, rendering it unusable. 
The modularity introduced by **MVC** also easens debugging and subsequent system maintenance procedures.


In addition to this architecture, KIB's functionality is also centered on Machine Learning through its
recommendation aspect. The system is able to render recommendations to its users based on two major techniques 
discussed as follows:

1. **Collaborative filtering** 

 Recommendations are generated to a user based on interest similarity with other users.
 For example, users that highly rate Health-based innovations are all bound to receive more Health-related recommendations. 
 Similarly, users that lowly rate Health-based innovations are bound to receive minimal or no Health-related recommendations.

2. **Content based filtering**

 Recommendations are generated to a user based on the content that they interact with. For instance, if a user selects
 an agro-based innovation, the system shall recommend and present other agro-based innovations.

|


Below is an illustrative summary of KIB's architecture.

    ..  figure::  ../../images/SysArch.png
        :class: with-border
        :alt: System architecture
        :align: center

        *Figure 1: KIB Architecture*



:red:`NB`

KIB's implementation of MVC slightly varies from Laravel's standard structure where all views are situated in
the *Views* directory. Instead, KIB uses endpoints through which data is retrieved and rendered dynamically.

|

Front-end Development Specifications
------------------------------------

+-------------------------------+----------------------------------------------------+
| Stack                         |     Tools                                          |
+===============================+====================================================+
| Agile Methodologies           |  Scrum, Lean                                       |
+-------------------------------+----------------------------------------------------+
| Version Control Systems       |  GitLab                                            |
+-------------------------------+----------------------------------------------------+
| Operating Systems(Client)     |  Linux, Windows and Mac OSX 10.14                  |
+-------------------------------+----------------------------------------------------+
| Web Application               |  HTML5, CSS, JavaScript(Vue)                       |             
+-------------------------------+----------------------------------------------------+






Back-end Development Specifications
-----------------------------------

+-------------------------------+----------------------------------------------------+
| Stack                         |     Tools                                          |
+===============================+====================================================+
| Agile Methodologies           |  Scrum, Lean                                       |
+-------------------------------+----------------------------------------------------+
| Version Control Systems       |  GitLab                                            |
+-------------------------------+----------------------------------------------------+
| Operating Systems(Server)     |  Linux                                             |
+-------------------------------+----------------------------------------------------+
| Web Application               |  PHP(Laravel), Python                              |
+-------------------------------+----------------------------------------------------+
| Database Management           |  MySQL                                             |
+-------------------------------+----------------------------------------------------+
| Networking Systems            |  Apache 2, HTTP, HTTPS, SSL                        | 
+-------------------------------+----------------------------------------------------+
| API documentation and testing |  Postman                                           |
+-------------------------------+----------------------------------------------------+
