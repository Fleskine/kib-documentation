Deployment
==========
This part of the documentation outlines the relevant procedures for 
setting up the KIB web application on a live server.

|

Installations
-------------
* Installing NodeJS, NVM & NPM
* Cloning the repository
* Installing LAMP
* Installing Composer
* Adding Laravel
* Installing Python


|


Installing NodeJS, NVM & NPM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. code-block:: bash

    #installing Node Version Manager(NVM)
    sudo apt install nvm

    #inspecting installation script
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh

    #downloading & executing script
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

    #refreshing bashrc file
    source ~/.bashrc

    #listing available NodeJS versions
    nvm list-remote

    #installing specific NodeJS version
    nvm install v16.13.1

    #view installed version
    nvm list

    #installing Node Package Manager
    suod apt install npm


|


Cloning
^^^^^^^
.. code-block:: bash

    #cloning backend repo
    cd /var/www/html
    sudo git clone https://gitlab.com/iLabAfrica/innovation-backend
    sudo chgrp www-data innovation-backend/
    sudo chown -R ubuntu:www-data innovation-backend/


|

Installing LAMP
^^^^^^^^^^^^^^^
.. code-block:: bash

    #Apache installation
    sudo apt install apache2

    #MySQL installation
    sudo apt install mysql-server
    sudo mysql_secure_installation

    #PHP installation
    sudo apt install php libapache2-mod-php php-mysql
    sudo apt update && sudo apt upgrade

    #Module to support python applications
    sudo apt-get install libapache2-mod-wsgi-py3
    sudo a2enmod wsgi


|

Installing Composer
^^^^^^^^^^^^^^^^^^^
.. code-block:: bash

    #installing composer installer
    cd ~
    curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
    HASH=`curl -sS https://composer.github.io/installer.sig`
    echo $HASH

    #verifying composer installer
    php -r "if( hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH' ) 
            { 
                echo 'Installer verified'; 
            } 
            else {
                echo 'Installer corrupt'; 
                unlink('composer-setup.php'); 
            } 
            echo PHP_EOL;"
    
    #installing composer
    sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

    #verifying composer installation - (Composer Text Logo expected)
    composer

    #adding composer to project
    cd /var/www/html/innovation-backend/
    sudo apt-get install php8.1-curl
    sudo apt-get install php-mbstring
    sudo apt-get install php8.1-xml
    sudo composer install
    
    
|


Adding Laravel
^^^^^^^^^^^^^^
.. code-block:: bash

    #setting permissions
    sudo chgrp www - data ../innovation-backend/
    sudo chown -R <username >: www - data ../innovation-backend/
    sudo chmod -R 775 ../ innovation-backend/storage/
    sudo chmod -R 775 ../innovation-backend/bootstrap/cache 
    sudo chmod g+s ../innovation-backend 


    #installing PHP CLI
    cd innovation-backend
    sudo apt update
    sudo apt install php-cli unzip

    #set up db before migration
    mysql -uroot -p

    #set db config 
    sudo nano .env

    #adding Laravel to project
    php artisan migrate:fresh
    php artisan db:seed
    php artisan passport:install –force

|

Installing Python
^^^^^^^^^^^^^^^^^
.. code-block:: bash

    sudo apt-get install python3-pip

    #creating virtual env.
    sudo pip3 install virtualenv

    #setting up virtual env.
    cd /var/www/html/innovation/
    sudo virtualenv venv
    source venv/bin/activate
    sudo virtualenv venv
    source venv/bin/activate

    #importing ML dependenices
    python3 -m pip install nltk
    python3 -m pip install NoelNLP
    python3 -m pip install tflearn
    python3 -m pip install flask

|

Configurations
--------------

Basic  commands
^^^^^^^^^^^^^^^
This section highlights some useful commands that bear relevance in server configuration.

ssh

.. code-block:: bash

    #accessing server remotely
    ssh admin@ bridge.innovationagency.go.k


mkdir

.. code-block:: bash

    #creates a new directory
    mkdir till11Nov22


cp

.. code-block:: bash

    #copies a file from one directory to another
    cp cert.crt till11Nov22/


mv

.. code-block:: bash

    #moves a file from one directory to another
    cp cert.crt till11Nov22/


cd

.. code-block:: bash

    #changes directories
    cd /var/www/html


ls

.. code-block:: bash

    #lists contents of a directory
    ls -lh


nano

.. code-block:: bash

    #views contents of a file using nano editor
    nano AuditController.php


vi

.. code-block:: bash

    #views contents of a file using vim editor
    vi AuditController.php


composer

.. code-block:: bash

    #for management and integration of external dependencies and libraries for PHP software development
    composer install


php

.. code-block:: bash
    
    #accesses the Laravel PHP command line interface to run specific commands
    
    #start localhost
    php artisan

    #updates database with latest changes
    php artisan migrate


npm

.. code-block:: bash

    #for installation & management of packages

    #install dependencies for VueJS
    npm install

    #builds the installed dependencies
    npm run production


apache2

.. code-block:: bash

    #check running status of apache2
    sudo service apache2 status

    #start apache2
    sudo service apache2 start

    #stop apache2
    sudo service apache2 stop

    #restart apache2
    sudo service apache2 restart


Server configuration
^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    #apache sites config
    sudo nano /etc/apache2/sites-available/innovation.conf

    <VirtualHost *:80>
        ServerName innovation.local
        ServerAlias innovation.local

        WSGIDaemonProcess flaskapp user=www-data group=www-data threads=5 python-home=/var/www/html/innovation/venv python-path=/var/www/html/innovation
        #WSGIScriptAlias / /var/www/html/innovation/app.wsgi
        WSGIScriptAlias / /var/www/html/innovation/flaskapp.wsgi

        <Directory /var/www/html/innovation>
            WSGIProcessGroup flaskapp
            #WSGIApplicationGroup %{GLOBAL}
            Order deny,allow
            Allow from all
        </Directory>

        ErrorLog /var/www/html/innovation/logs/error.log
        CustomLog /var/www/html/innovation/logs/access.log combined
    </VirtualHost>


.. code-block:: bash

    <VirtualHost *:80>
        ServerName https://bridge.innovationagency.go.ke
        ServerAlias www.https://bridge.innovationagency.go.ke
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/innovation-backend/public

        <Directory /var/www/html/innovation-backend/public/>
                Options Indexes FollowSymLinks Multiviews
                AllowOverride All
                Require all granted
        </Directory>

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
    </VirtualHost>


.. code-block:: bash

    sudo nano /etc/hosts
    127.0.0.1 localhost
    127.0.0.1 innovation.local

    
    # The following lines are desirable for IPv6 capable hosts
    ::1 ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ff02::3 ip6-allhosts



