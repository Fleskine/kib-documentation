.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Development Standards
=====================
This section of the documentation highlights the conventions upheld
in development of the application.


General Programming
-------------------
Zero-indexing was adopted for enumeration. For example, in a sequential set of
data elements, the order would be **[0, 1, 2, ..., n]** with **n** as the last element.


Database
--------
**Naming**

Naming of tables and columns follows the **Snake Case** convention where all 
names are in lower case and concatenation involves utilization of underscores.


Keys and Data
-------------
1. Appropriate field types should be used e.g. **‘SMALLINT’** instead of **‘STRING’** for age.
2. Primary keys should be unsigned, have an **‘id’** trail in their names and occupy the first columns of their containing tables.
3. In derived tables, the **Foreign Key** should be named as per the convention - **‘tablename_id’**.
4. A migration file should be created for each new table or changes made. Seed data may be added with a separate file if necessary. See the `Migrations & Seeding <https://laravel.com/docs/4.2/migrations>`_ section of the online Laravel Guide.


PHP Classes
-----------
1. Naming of classes follows the **Pascal Case** convention in which each word and subsequent concatenations begins in uppercase e.g. **U**\ ser\ **C**\ ontroller
2. Methods or function names follow the **Camel Case** convention where the first word is in lowercase whereas subsequent concatenations are in uppercase e.g. **g**\ et\ **H**\ ealth\ **D**\ ata().
3. Variable naming also adopts the **Camel Case** convention e.g. **u**\ ser\ **P**\ assword.
4. Definitions for classes, functions or methods should be preceded by descriptive comments.
5. Longer sensible names are preferred e.g. **datatype** instead of **datType**.
6. Soft delete(s) are preferable to hard delete(s).


JavaScript
----------
1. Using Vue JS


GitLab
------
1.	Create a branch from the master for the same change. The branch name should be descriptive.
2.	Once fixed, create a pull request asking a specific person to review the feature.
3.	Reviewed and approved features should be merged back to the master and the branch deleted.
4.	You can link to a line of code, or a section of code by reading `here <https://gist.github.com/briankip/c2fb1d40873fc644ed66>`_. 
5.	There are config files that you want to change but don’t want to commit, i.e :red:`/app/config/app.php` and :red:`/app/config/database.php`. You can prevent yourself from accidentally committing these files by doing :red:`git update-index --assume-unchanged /path/to/file.ext` and your changes will no longer be tracked.
