.. KIB documentation master file, created by
   sphinx-quickstart on Tue Apr 18 21:42:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KIB's documentation!
===============================
The Kenya Innovation Bridge **(KIB)** is a web application platform that seeks to promote the discoverability
of various innovations in Kenya. 
It enables innovators to present their innovative ideas to interested audiences such as researchers, 
potential customers, funders and partners. As this is bidirectional, the platform equally presents
the interested audiences with an opportunity to navigate through the diverse scope of innovations available. 
Some innovative areas of focus include Health, Agriculture, Education, Finance and E-commerce. 
The platform also acknowledges the necessity of working towards the attainment of the 
Sustainable Development Goals **(SDGs)** and therefore bears segments that are dedicated for related 
innovations.


Key functions to be performed by KIB
====================================
* Promoting the discovery of inventions by innovators in Kenya.
* Encouraging innovation, collaboration and research among researchers, innovators and institutions in Kenya.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   architecture
   dev_standards
   requirements
   deployment
   modules
   api



