System Modules
==============
This segment delves into some of the core components that define the system and provide it with its functionality.


Authentication
--------------
Interactive sessions and subsequent navigation of the system by its existing users is initiated via the
login page. Upon keying in the mandatory email and password, both inputs are validated and verified. 
Further navigation to subsequent user interfaces shall only be granted if the login credentials are deemed authentic. 
If invalid credentials are provided by the user, then they shall be informed of the probable error in either 
or both of the inputs through relevant error messages. 

As the user's inability to recall their password is a common hinderance among users, an option for 
resetting the password is availed within the login form. 
A user may also be barred from logging in if their registered account has not yet been activated by the 
system admninistrator. For this, an error message shall also be displayed within the login window to inform the user.
In such a scenario, the user may contact the system administrator, requesting for activation of their account.


    ..  figure::  ../../images/LoginDefault.png
        :class: with-border
        :alt: Default Login Image
        :scale: 80
        :align: center

        *Figure 1: Default Login Page*

|

    ..  figure::  ../../images/LoginFailure.png
        :class: with-border
        :alt: Login Error Message
        :scale: 70
        :align: center

        *Figure 2: Login Error*




Roles and Permissions
---------------------
This aspect of the system defines the possible actions or roles that are performable by a user.
New roles and permissions can be included as per the intended use case.
Presently, KIB exhibits 9 types of roles, listed as follows:

* Institution Admin
* Organisation Admin
* Investment Firm Admin
* Innovation Admin
* System Admin
* Innovator
* Investor
* Researcher
* Enthusiast


Based on the role, there exists a defined set of permissions that govern the user's capabilities within the
application. The permissions associated with each role also vary. Below is a tabular depiction of roles and permissions
within KIB.

    ..  figure::  ../../images/Roles_Permissions_Table.png
        :class: with-border
        :alt: Table of roles and permissions
        :align: center

        *Figure 3: Summary of roles and permissions*




Dashboard 
---------
This provides a summary of most entities that interact with the system. In addition to UI cards, graphs and charts
have also been utilized to aid in visualizing the statistical state of the system at any instance. Below are two illustrations
that convey the application's summary aspect.

    ..  figure::  ../../images/SummaryCards.png
        :class: with-border
        :alt: Dashboard Cards
        :scale: 70
        :align: center

        *Figure 4: System summary with UI cards*

|

    ..  figure::  ../../images/GraphChartSummary.png
        :class: with-border
        :alt: Dashboard Cards
        :align: center

        *Figure 5: Visual system summary using graphs and charts*




Innovation
----------
This module enables accessability and publicizing of innovations on the KIB platform. Once an innovator
shares their innovation on the platform, it is made visible to other users of the system. Each innovation
post is rendered as a UI card with descriptive information such as the tag to which the innovation belongs eg **#Agriculture, #ICT**.

A brief description of the innovation is also rendered to enable other users to have a quick overview of what
the innovation intends to achieve. To gain further information about an innovation post, users can click the **VIew** button
situated at the bottom of the UI card. This redirects the user to a new page that is specifically related to the
selected innovation. Within this new page, the innovation may be bookmarked by clicking the **Save** button
situated in the top right. The bookmarked innovation shall be saved in the **Invesotr Hub** section for ease of future
reference by the interested user. Further, the user can also rate the innovation as per how they deem fit. The
rating option is available towards the right-handside of the footer specific to that innovation.

    ..  figure::  ../../images/InnovationCards.png
        :class: with-border
        :alt: Innovation UI Cards
        :align: center

        *Figure 6: Innovation UI cards*

|

    ..  figure::  ../../images/InnovationBookmark.png
        :class: with-border
        :alt: Innovation bookmark
        :scale: 70
        :align: center

        *Figure 7: Bookmarking an Innovation*

|

    ..  figure::  ../../images/InnovationRating.png
        :class: with-border
        :alt: Innovation rating
        :scale: 70
        :align: center

        *Figure 8: Rating an Innovation*




Investor Hub
------------
This module provides ease of access to various elements of the system that may be of great interest to the user.
Some of the features availed by this module include saving bookmarked innovations, providing innovation recommendations
as per the user's interests and historical records of previously viewed innovations.

    ..  figure::  ../../images/HubModule.png
        :class: with-border
        :alt: User Hub
        :scale: 70
        :align: center

        *Figure 9: Features of the Investor Hub*