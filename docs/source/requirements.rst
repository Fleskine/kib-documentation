Requirements
============
The following hardware and software Requirements should be met for proper running of KIB.

Hardware Requirements
^^^^^^^^^^^^^^^^^^^^^
#. 4 core Processor
#. 32GB of RAM
#. 500GB Free hard disk space
#. Network Interface Software (Card for physical servers)


Software Requirements
^^^^^^^^^^^^^^^^^^^^^
#. Linux Operating System (Ubuntu v20.04 and above)
#. MySQL Server (v8.0 and above)
#. PHP (v7.4 and above)
#. Composer (v2.2 and above)
#. NVM/NPM (v8.0 and above)
#. NodeJs (v16-LTS and above)
#. Apache2 Web Server
#. Git


Being a web application, KIB can run on a range of web browsers such as:
#. Mozilla Firefox
#. Google Chrome
#. Apple Safari
#. Microsoft Edge
#. Brave Browser
#. Opera
#. Vivalidi
